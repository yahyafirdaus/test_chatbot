var pp_obot = document.getElementById('pp_obot').innerHTML;
var pp_user = document.getElementById('pp_user').innerHTML;

class Chat
{
  init()
  {
    this.check_token()
  }

  check_token()
  {
    var that = this
    let url = document.getElementById('server_link').value + `/account/api/check-token/`;
    fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({token:tmp_token})
      }).then(response => response.json())
      .then(result => {
        if (result.message == 'exist'){
          that.initial_chat()
          that.send_handler()
          that.logout_handler()
        } else {
          location.href="/account/login/?exist=no"
        }
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }

  initial_chat()
  {
    var element = document.getElementById('message_box');

    setTimeout(function() {
      element.innerHTML = 
      `
      <div class="direct-chat-msg">
        <div class="direct-chat-infos clearfix">
        </div>
        <img class="direct-chat-img" src="`+pp_obot+`" alt="message user image">
        <div class="direct-chat-text">
          Halo, Selamat datang!
        </div>
      </div>
      `;
    }, 1000);

    setTimeout(function() {
      let tmp = element.innerHTML;
      tmp += 
      `
      <div class="direct-chat-msg" style="margin-top: 20px;">
        <div class="direct-chat-infos clearfix">
        </div>
        <img class="direct-chat-img" src="`+pp_obot+`" alt="message user image">
        <div class="direct-chat-text">
          Ada yang bisa obot bantu?
        </div>
      </div>
      `;
      element.innerHTML = tmp;
    }, 2000);
  }

  send_handler()
  {
    var that = this;
    $("#message").keypress(function (e) {
        if(e.which === 13 && !e.shiftKey) {
            e.preventDefault();
            that.send_message();
        }
    });
    document.getElementById('send').addEventListener('click', ()=>{
      that.send_message()
    });
  }

  send_message()
  {
    let element = document.getElementById('message_box');
    let message = document.getElementById('message');
    let tmp = element.innerHTML;
    tmp += 
    `
    <div class="direct-chat-msg right" style="margin-top: 20px;">
      <div class="direct-chat-infos clearfix">
      </div>
      <img class="direct-chat-img" src="`+pp_user+`" alt="message user image">
      <div class="direct-chat-text">
        `+message.value+`
      </div>
    </div>
    `;
    element.innerHTML = tmp;
    this.get_response(message.value)
    message.value = '';
    message.innerHTML = '';
  }

  get_response(message)
  {
    let url = document.getElementById('server_link').value + `/chat/api/response/`;
    fetch(url, {
      method: "POST",
      headers: {
        'Authorization': 'Token '+tmp_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({message:message})
      }).then(response => response.json())
      .then(result => {
        var element = document.getElementById('message_box');

        setTimeout(function() {
          let tmp = element.innerHTML;
          tmp += 
          `
          <div class="direct-chat-msg" style="margin-top: 20px;">
            <div class="direct-chat-infos clearfix">
            </div>
            <img class="direct-chat-img" src="`+pp_obot+`" alt="message user image">
            <div class="direct-chat-text">
              `+result.response+`
            </div>
          </div>
          `;
          element.innerHTML = tmp;
        }, 1000);
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }

  logout_handler()
  {
    var that = this;
    document.getElementById('logout-btn').addEventListener('click', ()=>{
      that.logout()
    });
  }

  logout()
  {
    let url = document.getElementById('server_link').value + `/account/api/logout/`;
    fetch(url, {
      method: "POST",
      headers: {
        'Authorization': 'Token '+tmp_token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({token:tmp_token})
      }).then(response => response.json())
      .then(result => {
        if(result.status == 200){
          location.href="/account/login/?exist=no"
        } else {
          alert('Gagal melakukan logout')
        }
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }
}

let chat = new Chat();
chat.init();
