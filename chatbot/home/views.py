from django.shortcuts import render
from django.views.generic import View
from django.core.cache import caches

from chatbot.settings import SERVERLINK


class HomeViews(View):

    @classmethod
    def get(cls, request):

        context = {
            'pp_obot' : 'https://image.freepik.com/free-vector/cute-robot-cartoon-vector-icon-illustration-techology-robot-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-1474.jpg',
            'pp_user' : 'https://www.freeiconspng.com/uploads/png-button-question-mark-icon-26.png',
            'server_link': SERVERLINK,
            'token': request.session.get('token'),
        }

        return render(request,
            'home/home.html', context)