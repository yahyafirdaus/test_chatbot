from django.db import models

# Create your models here.
class QA(models.Model):
    pertanyaan = models.TextField(blank=True, null=True)
    jawaban = models.TextField(blank=True, null=True)
    tagger = models.TextField(blank=True, null=True)
