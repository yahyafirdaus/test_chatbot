# django
from django.views import View
from django.db.models import Max

# third party apps
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from chat.models import QA


class ResponseApi(APIView):
    @classmethod
    def get(self, request):
        # Cek hak akses
        user = True
        if not user:
            return Response({'status' : 401}, status=status.HTTP_401_UNAUTHORIZED)

        response = QA.objects.filter().values()

        return Response({'status' : 200, 'response' : response}, status=status.HTTP_200_OK)
