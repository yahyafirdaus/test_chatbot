# python
import os
from nltk.tag import CRFTagger

# django
from django.conf import settings
from django.views import View
from django.db.models import Max

# third party apps
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from chat.models import QA


class GenerateTaggerApi(APIView):
    @classmethod
    def get(self, request):
        list_qa = QA.objects.filter().values()
        for item in list_qa:
            tmp = item['pertanyaan'].lower()
            tmp = tmp.replace("?", "")
            tmp = tmp.replace(",", "")
            tmp = tmp.replace(".", "")
            tmp = tmp.replace('"', "")
            tmp = tmp.replace("'", "")
            tmp = tmp.replace(";", "")
            tmp = tmp.replace(":", "")
            tmp = tmp.replace("(", "")
            tmp = tmp.replace(")", "")
            tmp = tmp.replace("%", "")
            tmp = tmp.replace("$", "")
            tmp = tmp.replace("@", "")
            ct = CRFTagger()
            ct.set_model_file(os.path.join(settings.MEDIA_ROOT, 'all_indo_man_tag_corpus_model.crf.tagger'))
            hasil = ct.tag_sents([tmp.split()])
            tagger = ''
            for i in hasil[0]:
                tagger += ' ' if tagger != '' else ''
                tagger += list(i)[1]
            QA.objects.filter(id=item['id']).update(tagger=tagger, pertanyaan=tmp)

        return Response({'status' : 200, 'list_qa' : list_qa}, status=status.HTTP_200_OK)
