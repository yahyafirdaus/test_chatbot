import simplejson as json
import logging
import traceback
import os
from nltk.tag import CRFTagger

from django.conf import settings
from django.views.generic import View
from django.http import HttpResponse, JsonResponse


class ResponseApis(View):
    @classmethod
    def post(cls, request):
        message = json.loads(request.body)

        try:
            # text = open(os.path.join(settings.MEDIA_ROOT, 'note.txt'), 'rb').read()
            # print(text)
            text = [i for i in message['message'].split()]
            print(text)
            ct = CRFTagger()
            ct.set_model_file(os.path.join(settings.MEDIA_ROOT, 'all_indo_man_tag_corpus_model.crf.tagger'))
            hasil = ct.tag_sents([text])
            print(hasil)
        except Exception as e:
            print("error tag : ", e)

        try:
            response = "Maaf masih belum mengerti :("
            if message['message'] == "apa kabar":
                response = "Alhamdulillah baik hehe"
        except Exception as e:
            print(e)

        return JsonResponse({'response': response})
