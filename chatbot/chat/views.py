from django.shortcuts import render
from django.views.generic import View


class ChatViews(View):

    @classmethod
    def get(cls, request):

        context = {
            'message' : 'Halo!',
            'pp_obot' : 'https://image.freepik.com/free-vector/cute-robot-cartoon-vector-icon-illustration-techology-robot-icon-concept-isolated-premium-vector-flat-cartoon-style_138676-1474.jpg',
            'pp_user' : 'https://www.freeiconspng.com/uploads/png-button-question-mark-icon-26.png',
        }

        return render(request,
            'chat/new_page.html', context)