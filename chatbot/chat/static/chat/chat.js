var pp_obot = document.getElementById('pp_obot').innerHTML;
var pp_user = document.getElementById('pp_user').innerHTML;

class Chat
{
  init()
  {
    this.initial_chat()
    this.send_handler()
  }

  initial_chat()
  {
    var element = document.getElementById('message_box');

    setTimeout(function() {
      element.innerHTML = 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Halo, Selamat datang!</div>
      </div>`;
    }, 1000);

    setTimeout(function() {
      let tmp = element.innerHTML;
      tmp += 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Ada yang bisa obot bantu?</div>
      </div>`;
      element.innerHTML = tmp;
    }, 2000);
  }

  send_handler()
  {
    var that = this;
    $("#message").keypress(function (e) {
        if(e.which === 13 && !e.shiftKey) {
            e.preventDefault();
            that.send_message();
        }
    });
    document.getElementById('send').addEventListener('click', ()=>{
      that.send_message()
    });
  }

  send_message()
  {
    let element = document.getElementById('message_box');
    let message = document.getElementById('message');
    let tmp = element.innerHTML;
    tmp += 
    `<div class="d-flex justify-content-end mb-4">
      <div class="msg_cotainer_send">`+message.value+`</div>
      <div class="img_cont_msg">
        <img src="`+pp_user+`" class="rounded-circle user_img_msg">
      </div>
    </div>`;
    element.innerHTML = tmp;
    this.get_response(message.value)
    message.value = '';
    message.innerHTML = '';
  }

  get_response(message)
  {
    let params = this.extractParams();
    let url = this.buildUrl(asset(`/chat/apis/response/`), params);
    fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFTOKEN': document.getElementById('csrf_token').innerHTML
      },
      body: JSON.stringify({message:message})
      }).then(response => response.json())
      .then(result => {
        var element = document.getElementById('message_box');

        setTimeout(function() {
          let tmp = element.innerHTML;
          tmp += 
          `<div class="d-flex justify-content-start mb-4">
            <div class="img_cont_msg">
              <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
            </div>
            <div class="msg_cotainer">`+result.response+`</div>
          </div>`;
          element.innerHTML = tmp;
        }, 1000);
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }

  buildUrl(pathname, params) {
    // jika ada parameter tanpa value, hapus parameter tersebut
    Object.keys(params).forEach((key) => {
      if (params[key] === null) {
        delete params[key];
      }
    });

    // jika ternyata tidak ada parameter apapun, no process anymore
    if (Object.keys(params).length == 0) {
      return pathname;
    }

    return pathname + '?' + Object.keys(params).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&')
  }
  
  extractParams(currentUrl=window.location.href) {
    // check whether parameters exist by searching for ?
    // var currentUrl = window.location.href;

    // when no parameter found, return empty dict
    if (currentUrl.indexOf('?') < 0) {
      return {};
    }

    currentUrl = currentUrl.replace('#', '');

    var paramsList = currentUrl.split('?')[1].split('&');

    var params = {};
    paramsList.forEach(function(i) {
      var key = i.split('=')[0];
      // decodeURI convert %20 to space
      var value = decodeURI(i.split('=')[1]);

      params[key] = value;
    });

    return params;
  }
}

const asset = (url) => {
  return '' + url;
};

let chat = new Chat();
chat.init();
