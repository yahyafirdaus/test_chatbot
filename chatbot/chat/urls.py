from django.urls import path

from chat import views
from chat.apis import response_apis, response_api, generate_tagger_api

app_name = 'chat'

urlpatterns = [
    path('', views.ChatViews.as_view(), name='chat'),
    path('apis/response/', response_apis.ResponseApis.as_view(), name='response-apis'),

    path('api/response/', response_api.ResponseApi.as_view(), name='response-api'),
    path('api/generate/', generate_tagger_api.GenerateTaggerApi.as_view(), name='generate-api'),
]
