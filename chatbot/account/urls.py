from django.urls import path
from . import views

app_name = 'account'

urlpatterns = [
    path('', views.LoginViews.as_view(), name ="login"),
    path('login/', views.LoginViews.as_view(), name ='login'),
    path('register/', views.RegisterViews.as_view(), name ='register'),
    # path('logout/', views.logout_user, name='logout'),
    # path('register/', views.register_user, name='register'),
    # path('edit_profile/', views.edit_profile, name='edit_profile'),
    # path('change_password/', views.change_password, name='change_password'),

    # path('api/login/', apis.login, name='login-api'),
    # path('api/logout/', apis.LogoutApi.as_view(), name='logout-api'),
    # path('api/register/', apis.register, name='register-api'),
]

