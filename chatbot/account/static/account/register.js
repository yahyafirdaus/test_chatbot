class Register
{
  init()
  {
    this.register_handler()
  }

  validation_input()
  {
    var element = document.getElementById('message_box');

    setTimeout(function() {
      element.innerHTML = 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Halo, Selamat datang!</div>
      </div>`;
    }, 1000);

    setTimeout(function() {
      let tmp = element.innerHTML;
      tmp += 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Ada yang bisa obot bantu?</div>
      </div>`;
      element.innerHTML = tmp;
    }, 2000);
  }

  register_handler()
  {
    var that = this;
    document.getElementById('register-btn').addEventListener('click', ()=>{
      let data = [];
      data.push(document.getElementById('username').value)
      data.push(document.getElementById('password1').value)
      data.push(document.getElementById('password2').value)
      data.push(document.getElementById('first_name').value)
      data.push(document.getElementById('last_name').value)
      data.push(document.getElementById('email').value)
      that.register(data)
    });
  }

  register(data)
  {
    let url = document.getElementById('server_link').value + `/account/api/register/`;
    fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username:data[0],
        password:data[1],
        first_name:data[3],
        last_name:data[4],
        email:data[5]
      })
      }).then(response => response.json())
      .then(result => {
        if (result.message == "success"){
          location.href="/account/login/?exist=no"
        } else {
          alert("Cek kembali inputan")
        }
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }
}

let register = new Register();
register.init();
