class Login
{
  init()
  {
    this.login_handler()
  }

  validation_input()
  {
    var element = document.getElementById('message_box');

    setTimeout(function() {
      element.innerHTML = 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Halo, Selamat datang!</div>
      </div>`;
    }, 1000);

    setTimeout(function() {
      let tmp = element.innerHTML;
      tmp += 
      `<div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
          <img src="`+pp_obot+`" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">Ada yang bisa obot bantu?</div>
      </div>`;
      element.innerHTML = tmp;
    }, 2000);
  }

  login_handler()
  {
    var that = this;
    document.getElementById('login-btn').addEventListener('click', ()=>{
      let username = document.getElementById('username').value
      let password = document.getElementById('password').value
      that.login(username, password)
    });
  }

  login(username, password)
  {
    let url = document.getElementById('server_link').value + `/account/api/login/`;
    fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({username:username, password:password})
      }).then(response => response.json())
      .then(result => {
        document.cookie = "token="+result.token+"; path=/account";
        location.href="/account/login/"
      })
      .catch((error) => {
        alert('Mohon periksa koneksi internet anda \n\n' + error)
      })
  }
}

let login = new Login();
login.init();
