from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import View
from django.core.cache import caches
from django.http import HttpResponseRedirect

from chatbot.settings import SERVERLINK
from .forms import SignUpForm, EditProfileForm 


class LoginViews(View):

    @classmethod
    def get(cls, request):

        if 'exist' in request.GET:
            if request.GET.get('exist') == 'no':
                del request.COOKIES['token']
                request.session['token'] = '-'
                response = HttpResponseRedirect('/account')
                response.delete_cookie('token')
                response = HttpResponseRedirect('/account/login')
                response.delete_cookie('token')
                response = HttpResponseRedirect('/')
                response.delete_cookie('sessionid')

        if 'token' in request.COOKIES:
            if request.COOKIES['token'] != '':
                request.session['token'] = request.COOKIES['token']
                return redirect(reverse_lazy('home:home'))

        context = {
            'page_login' : 'active',
            'title' : 'Masuk',
            'server_link': SERVERLINK
        }

        return render(request,
            'account/login.html', context)

class RegisterViews(View):

    @classmethod
    def get(cls, request):

        form = SignUpForm() 
        context = {
            'page_register' : 'active',
            'title' : 'Daftar',
            'server_link': SERVERLINK
        }

        return render(request,
            'account/register.html', context)