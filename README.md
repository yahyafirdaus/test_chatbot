# Tugas Test Prosa AI



## Setting Server Link

- [ ] Ubah variable SERVERLINK dan CLIENTLINK di server/server/settings.py sesuai port localhost yang dipakai
- [ ] Ubah variable SERVERLINK di chatbot/chatbot/settings.py sesuai port localhost yang dipakai

## Cara Menggunakan

- [ ] Lakukan register terlebih dahulu.
- [ ] Lakukan login untuk mendapatkan token.
- [ ] Chat siap digunakan.