from django.urls import path

from chat.apis import response_api, generate_tagger_api

app_name = 'chat'

urlpatterns = [
    path('api/response/', response_api.ResponseApi.as_view(), name='response-api'),
    path('api/generate/', generate_tagger_api.GenerateTaggerApi.as_view(), name='generate-api'),
]
