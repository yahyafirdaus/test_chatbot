# python
import re
import os
import requests
import simplejson as json
from nltk.tag import CRFTagger
from difflib import SequenceMatcher

# django
from django.views import View
from django.conf import settings
from django.db.models import Max

# third party apps
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from chat.models import QA, Gaul


class ResponseApi(APIView):
    @classmethod
    def post(cls, request):
        data = json.loads(request.body)
        message = data.get('message')
        list_qa = QA.objects.filter().values()
        response = "Maaf masih belum mengerti :("
        check = message.split("=")[0]
        
        try:
            if check == "gaul":
                list_qa = Gaul.objects.filter().values()
                message = message.split("=")[1]
                
            # Pembersihan teks dari simbol dan karakter
            message = message.lower()
            message = re.sub('[^A-Za-z0-9]+', ' ', message)

            if message and message != '':
                for qa in list_qa:
                    rate = SequenceMatcher(None, message, qa['pertanyaan']).ratio()
                    if float(rate)*100 > 70:
                        response = qa['jawaban']
                        break
        except Exception as e:
            print(e)

        return Response({'status' : 200, 'response' : response}, status=status.HTTP_200_OK)
