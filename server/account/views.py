from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash 
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib import messages 
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token

from .forms import SignUpForm, EditProfileForm 


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	is_exist = Token.objects.filter(user=instance).values().first()
	token = None
	if is_exist:
		token = is_exist['key']
	else:
		token = Token.objects.create(user=instance)
	return token


# Create your views here.
def home(request): 
	return render(request, 'account/home.html', {})


def login_user (request):
	if request.method == 'POST': #if someone fills out form , Post it 
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user is not None:# if user exist
			result = login(request, user)
			messages.success(request,('Youre logged in'))
			token = create_auth_token(settings.AUTH_USER_MODEL, user, True)
			return redirect('account:home') #routes to 'home' on successful login  
		else:
			messages.success(request,('Error logging in'))
			return redirect('account:login') #re routes to login page upon unsucessful login
	else:
		return render(request, 'account/login.html', {})


def logout_user(request):
	logout(request)
	messages.success(request,('Youre now logged out'))
	return redirect('account:home')


def register_user(request):
	if request.method =='POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			login(request,user)
			messages.success(request, ('Youre now registered'))
			return redirect('account:home')
	else: 
		form = SignUpForm() 

	context = {'form': form}
	return render(request, 'account/register.html', context)


def edit_profile(request):
	if request.method =='POST':
		form = EditProfileForm(request.POST, instance= request.user)
		if form.is_valid():
			form.save()
			messages.success(request, ('You have edited your profile'))
			return redirect('account:home')
	else: 		#passes in user information 
		form = EditProfileForm(instance= request.user) 

	context = {'form': form}
	return render(request, 'account/edit_profile.html', context)
	#return render(request, 'authenticate/edit_profile.html',{})


def change_password(request):
	if request.method =='POST':
		form = PasswordChangeForm(data=request.POST, user= request.user)
		if form.is_valid():
			form.save()
			update_session_auth_hash(request, form.user)
			messages.success(request, ('You have edited your password'))
			return redirect('account:home')
	else: 		#passes in user information 
		form = PasswordChangeForm(user= request.user) 

	context = {'form': form}
	return render(request, 'account/change_password.html', context)