import simplejson as json

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, update_session_auth_hash 
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib import messages 
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token

from .forms import SignUpForm, EditProfileForm 

# third party apps
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import authentication_classes, permission_classes, api_view


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
	is_exist = Token.objects.filter(user=instance).values().first()
	if is_exist:
		Token.objects.filter(user=instance).delete()
	token = Token.objects.create(user=instance)

	return token


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def login(request):
	data = json.loads(request.body)
	username = data.get('username')
	password = data.get('password')

	user = authenticate(request, username=username, password=password)
	message = ''
	token = None
	if user is not None: # if user exist
		token = create_auth_token(settings.AUTH_USER_MODEL, user, True)
		user = User.objects.filter(username=user).values().first()
		message = 'success'
	else:
		message = 'failed'

	return Response({'status' : 200, 'message' : message, 'token' : str(token), 'user' : user}, status=status.HTTP_200_OK)


class LogoutApi(APIView):
	@classmethod
	def post(cls, request):
		data = json.loads(request.body)

		try:
			Token.objects.filter(key=data['token']).delete()
		except:
			return Response({'status' : 417, 'message' : 'failed'}, status=status.HTTP_417_EXPECTATION_FAILED)

		return Response({'status' : 200, 'message' : 'success'}, status=status.HTTP_200_OK)
		

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def register(request):
	inputan = json.loads(request.body)
	data = {
		'username' : inputan.get('username'),
		'password1' : inputan.get('password'),
		'password2' : inputan.get('password'),
		'first_name' : inputan.get('first_name'),
		'last_name' : inputan.get('last_name'),
		'email' : inputan.get('email'),
	}
	form = SignUpForm(data)
	if form.is_valid():
		form.save()
		user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
		message = ''
		token = None
		if user is not None: # if user exist
			token = create_auth_token(settings.AUTH_USER_MODEL, user, True)
			user = User.objects.filter(username=user).values().first()
			message = 'success'
		else:
			message = 'failed'
	else:
		return Response({'status' : 417, 'message' : 'failed'}, status=status.HTTP_417_EXPECTATION_FAILED)

	return Response({'status' : 200, 'message' : message, 'token' : str(token), 'user' : user}, status=status.HTTP_200_OK)


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def check_token(request):
	data = json.loads(request.body)
	token = Token.objects.filter(key=data['token']).values().first()
	if token:
		message = 'exist'
	else:
		message = 'no_exist'

	return Response({'status' : 200, 'message' : message}, status=status.HTTP_200_OK)


	# def edit_profile(request):
	# 	if request.method =='POST':
	# 		form = EditProfileForm(request.POST, instance= request.user)
	# 		if form.is_valid():
	# 			form.save()
	# 			messages.success(request, ('You have edited your profile'))
	# 			return redirect('account:home')
	# 	else: 		#passes in user information 
	# 		form = EditProfileForm(instance= request.user) 

	# 	context = {'form': form}
	# 	return render(request, 'account/edit_profile.html', context)
	# 	#return render(request, 'authenticate/edit_profile.html',{})


	# def change_password(request):
	# 	if request.method =='POST':
	# 		form = PasswordChangeForm(data=request.POST, user= request.user)
	# 		if form.is_valid():
	# 			form.save()
	# 			update_session_auth_hash(request, form.user)
	# 			messages.success(request, ('You have edited your password'))
	# 			return redirect('account:home')
	# 	else: 		#passes in user information 
	# 		form = PasswordChangeForm(user= request.user) 

	# 	context = {'form': form}
	# 	return render(request, 'account/change_password.html', context)
