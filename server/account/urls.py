from django.urls import path
from . import views, apis

app_name = 'account'

urlpatterns = [
    path('api/login/', apis.login, name='login-api'),
    path('api/logout/', apis.LogoutApi.as_view(), name='logout-api'),
    path('api/register/', apis.register, name='register-api'),
    path('api/check-token/', apis.check_token, name='check-token'),
]

